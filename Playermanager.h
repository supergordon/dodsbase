#ifndef _PLAYERMANAGER_H_
#define _PLAYERMANAGER_H_

#include "SDK.h"

class CPlayerManager
{
	public:
		CBaseEntity* pGetEntity(int iPlayer);

		void DrawFilledQuad(int x, int y, int w, int h, int r, int g, int b, int a);
		void DrawQuad(int x, int y, int w, int h, int r, int g, int b, int a, int thickness);
		void drawString(bool bCenter, bool bBackground, int x, int y, int r, int g, int b, int a, const char *pInput, ...);
		bool WorldToScreen( const Vector &vOrigin, Vector &vScreen);
		bool GetBonePosition (int iBone, Vector& vecOrigin, QAngle qAngles, int index);
		void RegisterFont(vgui::HFont &font, char* szFont, int iSize, int iWidth, int iExtra);

		vgui::HFont gFont;
}; 

extern CPlayerManager gPlayerManager;

#endif