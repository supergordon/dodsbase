#include <windows.h>
#include "SDK.h"
#include "PlayerManager.h"

CPlayerManager gPlayerManager;


CBaseEntity* CPlayerManager::pGetEntity(int iPlayer)
{
	IClientEntity* pEntity = hl2.pEntityList->GetClientEntity(iPlayer);

	if (!pEntity || pEntity->IsDormant())
		return NULL;

	return pEntity->GetBaseEntity();
}

void CPlayerManager::DrawFilledQuad(int x, int y, int w, int h, int r, int g, int b, int a)
{
	hl2.pSurface->DrawSetColor(r,g,b,a);
    hl2.pSurface->DrawFilledRect(x,y,x+w,y+h);
}

void CPlayerManager::DrawQuad(int x, int y, int w, int h, int r, int g, int b, int a, int thickness)
{
	DrawFilledQuad(x,y,w,thickness,r,g,b,a); //oben
	DrawFilledQuad(x,y+h,w,thickness,r,g,b,a); //unten
	DrawFilledQuad(x,y,thickness,h,r,g,b,a); //links
	DrawFilledQuad(x+w,y,thickness,h+thickness,r,g,b,a); //rechts
}

bool ScreenTransform( const Vector &point, Vector &screen )
{
	float w;
	const VMatrix &worldToScreen = hl2.pEngineFuncs->WorldToScreenMatrix();
	screen.x = worldToScreen[0][0] * point[0] + worldToScreen[0][1] * point[1] + worldToScreen[0][2] * point[2] + worldToScreen[0][3];
	screen.y = worldToScreen[1][0] * point[0] + worldToScreen[1][1] * point[1] + worldToScreen[1][2] * point[2] + worldToScreen[1][3];
	w		 = worldToScreen[3][0] * point[0] + worldToScreen[3][1] * point[1] + worldToScreen[3][2] * point[2] + worldToScreen[3][3];
	screen.z = 0.0f;

	bool behind = false;

	if( w < 0.001f )
	{
		behind = true;
		screen.x *= 100000;
		screen.y *= 100000;
	}
	else
	{
		behind = false;
		float invw = 1.0f / w;
		screen.x *= invw;
		screen.y *= invw;
	}
	return behind;
}

bool CPlayerManager::WorldToScreen( const Vector &vOrigin, Vector &vScreen)
{
	if(!ScreenTransform(vOrigin , vScreen))
	{
		int iScreenWidth, iScreenHeight;

		hl2.pEngineFuncs->GetScreenSize( iScreenWidth, iScreenHeight );
		float x = iScreenWidth / 2;
		float y = iScreenHeight / 2;
		x += 0.5 * vScreen.x * iScreenWidth + 0.5;
		y -= 0.5 * vScreen.y * iScreenHeight + 0.5;
		vScreen.x = x;
		vScreen.y = y;
		return true;
	}
	return false;
}


void CPlayerManager::drawString(bool bCenter, bool bBackground, int x, int y, int r, int g, int b, int a, const char *pInput, ...)
{
	if( pInput == NULL )
		return;

	va_list va_alist;
	char szBuffer[1024] = "";
	wchar_t szString[1024] = { '\0' };

	va_start(va_alist, pInput);
	vsprintf(szBuffer, pInput, va_alist);
	va_end(va_alist);

	wsprintfW(szString, L"%S", szBuffer);

	hl2.pSurface->DrawSetTextPos(x, y);
	hl2.pSurface->DrawSetTextFont(gFont);
	hl2.pSurface->DrawSetTextColor(r, g, b, a);
	int wide, tall;

	int xx = x;
	int yy = y;

	hl2.pSurface->GetTextSize(gFont, szString, wide, tall);

	if(bCenter)
	{
		if(bBackground)
			DrawFilledQuad(xx-(wide/2)-2, yy, wide+4, 15, 0, 0, 0, 0x3F);
		hl2.pSurface->DrawSetTextPos(xx-(wide/2), yy);
	}

	hl2.pSurface->DrawPrintText(szString, wcslen(szString));
}


//bool C_BaseEntity::SetupBones( matrix3x4_t *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime )
//{
//	_asm JMP DWORD PTR [ ECX + 0x3C ];
//}

bool CPlayerManager::GetBonePosition (int iBone, Vector& vecOrigin, QAngle qAngles, int index)
{
	if( iBone < 0 || iBone >= 20 )
		return false;

	matrix3x4_t pmatrix[MAXSTUDIOBONES];

	CBaseEntity* pEntity = pGetEntity(index);
	
	if(!pEntity->SetupBones(pmatrix, 128, BONE_USED_BY_HITBOX, 1.0f))
		return false;

	MatrixAngles(pmatrix[iBone], qAngles, vecOrigin);

	return true;
}

void CPlayerManager::RegisterFont(vgui::HFont &font, char* szFont, int iSize, int iWidth, int iExtra)
{
	font  = hl2.pSurface->CreateFont();	
	hl2.pSurface->SetFontGlyphSet(font, szFont, iSize, iWidth, 0, 0, iExtra);
}