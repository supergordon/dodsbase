#include <windows.h>
#include <fstream>
#include "sdk.h"
#include "PlayerManager.h"

HL2 hl2;
DWORD *dwOldEnginefuncs = 0;
DWORD dwIsDrawingOld = 0;
DWORD dwIsDrawingNew = 0;


struct factorylist_t
{
	CreateInterfaceFn appSystemFactory;
	CreateInterfaceFn physicsFactory;
};

typedef void (__cdecl* FactoryList_Retrieve_t)( factorylist_t &destData );
FactoryList_Retrieve_t pFactoryList_Retrieve;

void Hook();
bool __stdcall IsDrawingLoadingImage();
bool __stdcall FireEvent(IGameEvent *a, bool b);

void LogText(const char* szText, ...)
{
	char szBuffer[1024];
	va_list args;
    va_start(args, szText);
    memset(szBuffer, 0, sizeof(szBuffer));
    _vsnprintf(szBuffer, sizeof(szBuffer) - 1, szText, args);
    va_end(args);

	std::ofstream LOG("C:\\hl2.txt",std::ios::app);
	LOG << szBuffer << std::endl;
	LOG.close();
}

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	if(fdwReason == 1)
	{
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Hook, 0, 0, 0);
	}
	else if(fdwReason == DLL_PROCESS_DETACH)
	{
		//
	}
	return true;
}

bool bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for(;*szMask;++szMask,++pData,++bMask)
		if(*szMask=='x' && *pData!=*bMask ) 
			return false;
	return (*szMask) == NULL;
}

DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask)
{
	for(DWORD i=0; i < dwLen; i++)
		if( bDataCompare( (BYTE*)( dwAddress+i ),bMask,szMask) )
			return (DWORD)(dwAddress+i);

	return 0;
}

DWORD dwGetActiveWeapon = 0;

C_BaseCombatWeapon* GetActiveWeapon()
{
	__asm call dwGetActiveWeapon
}

float GetWeaponSpread ( CBaseCombatWeapon * pWeapon, CBaseEntity* pLocal )
{
	float fResult = 0.0f;


	Vector m_vecVelocity = *(Vector*)((DWORD)pLocal	+ 0x0E8);
	int iSpeed;
	if( m_vecVelocity.IsLengthLessThan(1) )
		iSpeed = 0;
	else
		iSpeed = (int)sqrt(m_vecVelocity.x*m_vecVelocity.x + m_vecVelocity.y*m_vecVelocity.y);

	__asm
	{
		MOV ECX, pWeapon
		MOV ECX,DWORD PTR DS:[ECX+0x8DC]
		FLD DWORD PTR DS:[ECX+0x6CC]
		CMP iSpeed, 1
		JB FinalSpread
		FADD DWORD PTR DS:[ECX+0x6D4]
FinalSpread:
		FSTP fResult
	}
	return fResult;
}

void Hook()
{
	CreateInterfaceFn Client = NULL, Engine = NULL, VGUI = NULL;
	
	HMODULE hClient = NULL, hEngine = NULL, hVGUI = NULL;
	while(!hClient)
	{
		hClient = GetModuleHandle("client.dll");
		Sleep(50);
	}

	hEngine = GetModuleHandle("engine.dll"); //client.dll loaded -> engine.dll loaded
	//hVGUI = GetModuleHandle("vguimatsurface.dll");

	Client = (CreateInterfaceFn)GetProcAddress(hClient, "CreateInterface");
	Engine = (CreateInterfaceFn)GetProcAddress(hEngine, "CreateInterface");
	//VGUI   = (CreateInterfaceFn)GetProcAddress(hVGUI, "CreateInterface");

	hl2.pClientFuncs = (IBaseClientDLL*)Client(CLIENT_DLL_INTERFACE_VERSION, 0);
	hl2.pEngineFuncs = (IVEngineClient *)Engine(VENGINE_CLIENT_INTERFACE_VERSION, 0);
	hl2.pEnginetrace = (IEngineTrace*)Engine(INTERFACEVERSION_ENGINETRACE_CLIENT, 0);
	hl2.pEntityList	 = (IClientEntityList*)Client(VCLIENTENTITYLIST_INTERFACE_VERSION, 0);
	hl2.pEnginevgui	 = (IEngineVGui*)Engine("VEngineVGui001", 0);
	hl2.pModelInfo	 = (IVModelInfoClient*)Engine(VMODELINFO_CLIENT_INTERFACE_VERSION, 0);
	hl2.pDebugOverlay= (IVDebugOverlay*)Engine(VPHYSICS_DEBUG_OVERLAY_INTERFACE_VERSION, 0);

	factorylist_t factories;
																				//E8 ?? ?? ?? ?? 85 C0 75 ?? C3 8B 10 8B C8 8B 82
	
	dwGetActiveWeapon = (DWORD)dwFindPattern((DWORD)hClient, 0x0058C000, (BYTE*)"\xE8\x00\x00\x00\x00\x85\xC0\x75\x00\xC3\x8B\x10\x8B\xC8\x8B\x82\x00\x00\x00\x00\xFF\xE0", "x????xxx?xxxxxxx????xx");
	
	
	DWORD dwFactory = NULL;
	while(!dwFactory)
	{
		dwFactory = dwFindPattern( ( DWORD ) hClient + 0x1000, 0x24FFFFFF - 0x24001000, ( PBYTE )"\x8B\x44\x24\x00\x8B\xD\x00\x00\x00\x00\x8B\x15\x00\x00\x00\x00\x89\x8\x89\x50\x00\xC3", "xxx?xx????xx????xxxx?x" );
		Sleep(50);
	}
	Sleep(7000);

	pFactoryList_Retrieve = (FactoryList_Retrieve_t)dwFactory;
	pFactoryList_Retrieve(factories);

	hl2.pSurface = (vgui::ISurface*)factories.appSystemFactory("VGUI_Surface030", NULL);

	dwOldEnginefuncs = (DWORD*)(*(DWORD*)hl2.pEngineFuncs);
	dwIsDrawingOld = dwOldEnginefuncs[28];
	dwIsDrawingNew = (DWORD)&IsDrawingLoadingImage;

	DWORD dwProtect;
	VirtualProtect((void*)(dwOldEnginefuncs+(28*4)), 4, PAGE_EXECUTE_READWRITE, &dwProtect);
	dwOldEnginefuncs[28] = dwIsDrawingNew;
	VirtualProtect((void*)(dwOldEnginefuncs+(28*4)), 4, dwProtect, 0);
}



int lala = 0;
int wtf = 0;
void ESP(int i)
{
	player_info_t pInfo = {0};
				
	CBaseEntity *pLocal = gPlayerManager.pGetEntity(hl2.pEngineFuncs->GetLocalPlayer());
	//gPlayerManager.drawString(0, 0, 10,10, 255, 255, 255, 255, "%X", pLocal);


	Vector vMeWorldPos, vMeEyeHeight;
	QAngle qAngles, qBla;
			
	if ( i == hl2.pEngineFuncs->GetLocalPlayer() )
		return;

	CBaseEntity* pEntity = gPlayerManager.pGetEntity(i);

	
	if (pEntity != NULL && !pEntity->IsDormant() && pEntity->m_lifeState == LIFE_ALIVE && hl2.pEngineFuncs->GetPlayerInfo( i, &pInfo ))
	{
		Vector vScreen, vWorldPos, vEntityEyeHeight, vBones, vCalcBones;

		if(!gPlayerManager.GetBonePosition(0/*0*/, vWorldPos, qAngles, i ))
			return;

		vWorldPos.z += 5;

		if(!gPlayerManager.WorldToScreen( vWorldPos, vScreen ))
			return;

		int r = 0; int g = 0; int b = 0; int a = 0;
		if(pEntity->m_iTeamNum == 2)
		{
			r = 0; g = 255; b = 0;
		}
		
		else if(pEntity->m_iTeamNum == 3)
		{
			r = 255; g = 2; b = 0;
		}
	
		gPlayerManager.DrawQuad(vScreen.x-5,vScreen.y-5, 10, 10, r, g, b, 255, 1);
		//gPlayerManager.drawString(true, 0, vScreen.x, vScreen.y, r, g, b, 255, "%s", pInfo.name);
		//gPlayerManager.drawString(true, 0, vScreen.x, vScreen.y+12, r, g, b, 255, "%d", pEntity->GetHealth());

	}
}

bool __stdcall IsDrawingLoadingImage()
{
	dwOldEnginefuncs[28] = dwIsDrawingOld;
	bool bRet = hl2.pEngineFuncs->IsDrawingLoadingImage();
	dwOldEnginefuncs[28] = dwIsDrawingNew;

	
	static bool bInit = false;
	if(!bInit)
	{
		//gPlayerManager.RegisterFont(gPlayerManager.gFont, "Courier New", 14, 450, 0x200);
		hl2.pEngineFuncs->ExecuteClientCmd("echo keke");
		bInit = true;
	}

	CBaseEntity *pLocal = gPlayerManager.pGetEntity(hl2.pEngineFuncs->GetLocalPlayer());
	if(pLocal) {
		C_BaseCombatWeapon* pWeapon = 0;
		pWeapon = GetActiveWeapon();
		if(pWeapon) {
			float fSpread = GetWeaponSpread(pWeapon, pLocal);
			char kekeke[200] = ""; sprintf(kekeke, "echo %X %X %.2f", pLocal,pWeapon, fSpread);
			hl2.pEngineFuncs->ExecuteClientCmd(kekeke);
		}
	}

	wtf = 0;
	for(int i = 0; i < 65; i++)
		ESP(i);

	gPlayerManager.DrawFilledQuad(10, 10, 10, 10, 255, 0, 0, 255);

	return bRet;
}